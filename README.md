# About this ReactJS Application

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It is a static app, that displays a list of movies, also providing you with an option to search by movie title.

## Hosting

The application is hosted using Firebase. Here is a link to the application https://my-movies-catalogue.web.app/

## Learning and Take Aways

**Function components**

**Hooks (useState useEffect)**

**JSX extension**

**Events**

**Props**

**API's**

**Firebase Hosting and Deployment**

**Environment Variables**

**API Key non-sensitive**

While there is still so much that can still be done to improve this application, I am keeping this to the barest minimum. Currently working on my portfolio to show case what I can do.

## Several Improvements

I could secure the application by using Access tokens but since the data being retrived is not sensitive, I decided not to implement it.

